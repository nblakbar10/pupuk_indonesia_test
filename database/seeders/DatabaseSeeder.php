<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        // $faker = Faker::create();
        
        // foreach (range(1,100) as $index) {
        //     DB::table('products')->insert([
        //         'code' => $this->$faker->asciify('***********'),
        //         'name' => $this->$faker->words(),
        //         'description' => $this->$faker->sentence(9),
        //         'image' => env('APP_URL').$this->faker->image('public/storage',640,480, null, false),
        //         'quantity' => $this->$faker->randomElement(['meter', 'pieces', 'doz', 'kilogram']),
        //         'stock' => $this->$faker->numberBetween(10, 100),
        //         'price' => $this->$faker->numberBetween(1000, 999999),
        //         'created_at' => $faker->date($format = 'd-m-Y H:i:s', $max = 'now')
        //     ]);
        // }
    }
}
